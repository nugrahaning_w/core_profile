<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    public function exampleMethod()
    {
        $app = app();
        $data = $app->make('stdClass');
            $data->title = 'sonarum';
            $data->header = 'Welcome to sonarum';
        return response()->json($data);
    }
    public function notfoundMethod()
    {
        // $app = app();
        // $data = $app->make('stdClass');
        //     $data->title = 'Not Found';
        //     $data->status = 404;
        //     $data->message = 'Not Found';
        // return response()->json($data);
        return view('welcome');
    }
}
